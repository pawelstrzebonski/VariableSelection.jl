@testset "utils.jl" begin
    res = 0

    terms = ["a", "b"]
    @test (res = VariableSelection.generate_variables(terms, 2); true)
    @test length(res) == 4
    @test (res = VariableSelection.generate_variables(terms, 3); true)
    @test length(res) == 6
    @test (res = VariableSelection.generate_variables(terms, 2, mixed = true); true)
    @test length(res) == 8
    @test (
        res = VariableSelection.generate_variables(terms, 2, mixed = true, mixedorder = 3); true
    )
    @test length(res) == 7
end
