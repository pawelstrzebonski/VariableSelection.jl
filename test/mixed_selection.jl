@testset "mixed_selection.jl" begin
    res = 0

    #TODO: Choose an example that may lead to variables being dropped?
    oterm = VariableSelection.GLM.term("c")
    iterm = VariableSelection.generate_variables(["a", "b"], 2)
    a = rand(100)
    b = rand(100)
    c = @. a + 2 * b + 3 * b^2 + 4
    data = VariableSelection.DataFrames.DataFrame([a, b, c], [:a, :b, :c])
    @test (res = VariableSelection.mixed_selection(oterm, iterm, data, 3); true)
    @test length(res) == 2
    @test length(res[1]) == length(res[2]) == 4
    @test iszero(res[2][1]) && isone(res[2][end])
end
