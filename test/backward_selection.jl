@testset "backward_selection.jl" begin
    res = 0

    oterm = VariableSelection.GLM.term("c")
    iterm = VariableSelection.generate_variables(["a", "b"], 3)
    a = rand(100)
    b = rand(100)
    c = @. a + 2 * b + 3b^2 + 4
    data = VariableSelection.DataFrames.DataFrame([a, b, c], [:a, :b, :c])
    @test (res = VariableSelection.backward_selection(oterm, iterm, data, 4); true)
    @test length(res) == 2
    @test length(res[1]) == length(res[2]) == 5
    @test !isone(res[2][end]) && isone(res[2][end-1])
end
