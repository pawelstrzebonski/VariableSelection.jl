import GLM

"""
    get_t(model)

Get t-values for a model.
"""
get_t(model) = GLM.coeftable(model).cols[3]

"""
    tlength(x)

Calculate the length of a StatsModels Term or InteractionTerm.
"""
tlength(x::GLM.StatsModels.Term) = 1
tlength(x::GLM.StatsModels.InteractionTerm) = sum(tlength.(x.terms))

"""
    generate_variables(
		variables::AbstractVector,
		order::Integer;
		mixed::Bool = false,
		mixedorder::Integer = 0,
	)

Given a list of `variables` (strings or symbols), return a collection of
all terms up to the specified `order`. If `mixed` is declared `true`, then
include the mixed interaction terms of those variables (and their higher order
forms). If `mixedorder` is not zero, then only terms with higher length
below that order will be returned.
"""
function generate_variables(
    variables::AbstractVector,
    order::Integer;
    mixed::Bool = false,
    mixedorder::Integer = 0,
)
    # Convert to Terms
    terms = GLM.term.(variables)
    # Generate a list of lists of powers of variables
    term_powers = [
        [GLM.StatsModels.InteractionTerm(tuple(fill(t, o)...)) for o = 1:order] for
        t in terms
    ]
    all_terms = reduce(vcat, term_powers)
    mixed_terms = []
    # If we want mixed terms...
    if mixed
        # Start with the first variable's terms...
        append!(mixed_terms, term_powers[1])
        # And for every subsequent variable
        for ts in term_powers[2:end]
            new_terms = []
            # Find all combinations of the next variable and all previous terms
            for t in ts
                for x in mixed_terms
                    newterm = GLM.StatsModels.InteractionTerm(tuple(t.terms..., x.terms...))
                    append!(new_terms, [newterm])
                end
            end
            # And append to the mixed terms list
            append!(mixed_terms, new_terms)
        end
        # Combine all of the terms lists
        append!(all_terms, reduce(vcat, mixed_terms))
    end
    # Try to remove repeats (not comprehensive as a&b is considered different from b&a)
    #TODO: Improve unique filtering of terms
    all_terms = unique(all_terms)
    # Select terms below the order limit
    all_terms =
        iszero(mixedorder) ? all_terms : filter(x -> tlength(x) <= mixedorder, all_terms)
    all_terms
end
