module VariableSelection

include("utils.jl")
include("forward_selection.jl")
include("backward_selection.jl")
include("mixed_selection.jl")

export generate_variables, forward_selection, backward_selection, mixed_selection

end
