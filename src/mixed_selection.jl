import GLM, DataFrames

"""
    mixed_selection(
		output::GLM.StatsModels.Term,
		inputs::AbstractVector,
		data::DataFrames.DataFrame,
		maxiter::Integer;
		tmin::Number = 1,
	)->(terms, R2s)

Given potential input terms `inputs` and output variable term `output`,
use mixed selection to choose which terms make a good model for the
provided `data` over `maxiter` iterations. Starting from no terms, it
will iteratively add the one to the list that gives the biggest improvement
in R2 versus the prior list of variables. If at any point any of the terms
has a t-value below `tmin`, it will be dropped from the list.
Returns a list of lists for the terms in each iteration, and the best
R2 value for each iteration.
"""
function mixed_selection(
    output::GLM.StatsModels.Term,
    inputs0::AbstractVector,
    data::DataFrames.DataFrame,
    maxiter::Integer;
    tmin::Number = 1,
)
    inputs = deepcopy(inputs0)
    # Define output arrays
    bestterms = Any[GLM.term(1)]
    r2s = [0.0]
    # Iterate for the desired number of fitting terms
    for _ = 1:maxiter
        # For each potential term
        perf = map(inputs) do t
            # Create formula with constant term, the prior best terms, and term under test
            f = output ~ bestterms[end] + t
            try
                # Try to fit the formula
                m = GLM.lm(f, data)
                # and evaluate R2 parameter
                GLM.r2(m)
            catch
                # Sometimes the formula is ill formed, in which case we return 0
                #@warn string("Error with ",f)
                0.0
            end
        end
        # Find the best term added
        bestt = inputs[argmax(perf)]
        # Add it to the list of best terms
        push!(bestterms, bestterms[end] + bestt)
        # And remove from inputs
        inputs = inputs[(1:length(inputs)).!==argmax(perf)]
        # And add the current top performance
        push!(r2s, maximum(perf))
        @info string("Adding ", bestt, " with R2=", maximum(perf))
        # Evaluate the model
        m = GLM.lm(output ~ bestterms[end], data)
        # Evaluate t-values for the terms
        ts = abs.(get_t(m))
        # Are any of the terms insignificant now?
        # Note: First term is intercept, which should be left alone
        # Note: Also ignore last term to avoid situation where we're adding and removing the same term repeatedly
        if any(ts[2:end-1] .< tmin)
            removed = bestterms[end][[false; ts[2:end-1] .< tmin; false]]
            # If so, remove the insignificant terms
            push!(bestterms, bestterms[end][[true; ts[2:end-1] .>= tmin; true]])
            # And re-add to inputs list
            append!(inputs, removed)
            f = output ~ bestterms[end]
            m = GLM.lm(f, data)
            push!(r2s, GLM.r2(m))
            @info string("Removing ", removed, " for R2=", r2s[end])
        end
    end
    # Return list of terms (in order) along with the evolving R2 performance
    bestterms, r2s
end
