import GLM, DataFrames

"""
    backward_selection(
		output::GLM.StatsModels.Term,
		inputs::AbstractVector,
		data::DataFrames.DataFrame,
		maxiter::Integer,
	)->(terms, R2s)

Given potential input terms `inputs` and output variable term `output`,
use backward selection to choose which terms make a good model for the
provided `data` over `maxiter` iterations. Starting with all terms, it
will iteratively remove the one to the list that gives the smallest degredation
in R2 versus the prior list of variables. Returns a list of lists for
the terms in each iteration, and the best R2 value for each iteration.
"""
function backward_selection(
    output::GLM.StatsModels.Term,
    inputs0::AbstractVector,
    data::DataFrames.DataFrame,
    maxiter::Integer,
)
    inputs = deepcopy(inputs0)
    # Define output arrays
    bestterms = Any[sum(inputs0)]
    r2s = Float64[GLM.r2(GLM.lm(output ~ bestterms[end], data))]
    # Iterate for the desired number of fitting terms
    for _ = 1:maxiter
        n = length(bestterms[end])
        # For each potential term
        perf = map(1:n) do i
            # Create formula with constant term, the prior best terms, and term under test
            f = output ~ bestterms[end][(1:n).!=i]
            try
                # Try to fit the formula
                m = GLM.lm(f, data)
                # and evaluate R2 parameter
                GLM.r2(m)
            catch
                # Sometimes the formula is ill formed, in which case we return 0
                #@warn string("Error with ",f)
                0.0
            end
        end
        # Find the best term added
        worstt = bestterms[end][argmax(perf)]
        # Add it to the list of best terms
        push!(bestterms, bestterms[end][(1:n).!=argmax(perf)])
        # And add the current top performance
        push!(r2s, maximum(perf))
        @info string("Removing ", worstt, " with R2=", maximum(perf))
    end
    # Return list of terms (in order) along with the evolving R2 performance
    bestterms, r2s
end
