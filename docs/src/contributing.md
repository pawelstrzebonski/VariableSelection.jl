# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Better examples of usage
* Optimizations
* More variable selection algorithms
* Better, more automated heuristics for variable selection

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support arbitrary numbers of variables and orders
* We aim to minimize the amount of code and function repetition
