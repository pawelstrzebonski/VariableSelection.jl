# backward_selection.jl

## Description

This file implements backward selection of variables.

## Functions

```@autodocs
Modules = [VariableSelection]
Pages   = ["backward_selection.jl"]
```
