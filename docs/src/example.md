# Example

To begin with, we will import the package:

```@example test
# Import package
import VariableSelection
```

Now, we must prepare our dataset as a `DataFrame`. The `DataFrame` must
have the columns named with the appropriate variable names (these will
be used later). In our example we have `c` be a function of `a` and `b`:

```@example test
# We generate some example data
a = rand(100)
b = rand(100)
# This is a toy example, so we know the relation between c and a/b
# In practice, this is what you're trying to find out
c = @. a + 2 * b + 3b^2 + 4
# We format the data as a DataFrame
data = VariableSelection.DataFrames.DataFrame([a, b, c], [:a, :b, :c])
nothing # hide
```

Next, we must generate a list of potential input terms, not just `a` and
`b` but any higher powers or mixed terms if we expect they may be part
of the model. We can do this manually, or we can use `generate_variables`
to do this for us. We provide a list of variable names and the upper limit
on variable order:

```@example test
# We generate a list of potential variables
iterm = VariableSelection.generate_variables(["a", "b"], 2)
```

If we wanted to include mixed terms (such as `a*b`), we can set the
appropriate options to `generate_variables` (see the documentation). Now
that we have the potential input terms, we must generate the output term
and call a variable selection routine. We start with forward selection:

```@example test
# Define the output variable
oterm = VariableSelection.GLM.term("c")
# Run forward selection of variables for 3 iterations
terms, R2s = VariableSelection.forward_selection(oterm, iterm, data, 3)
```

The variable selection routines will return two lists. The first is a list
of variable lists (results of each iteration) and the corresponding
R2 values for each iteration. For less trivial problems a mixed selection
algorithm (provided by the `mixed_selection` function) or
backward selection algorithm (`backward_selection`) be better.
