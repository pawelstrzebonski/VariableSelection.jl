# utils.jl

## Description

This file implements assorted utility functions, primarily related to
prepping lists of variable terms.

## Functions

```@autodocs
Modules = [VariableSelection]
Pages   = ["utils.jl"]
```
