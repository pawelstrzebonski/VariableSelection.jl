# forward_selection.jl

## Description

This file implements forward selection of variables.

## Functions

```@autodocs
Modules = [VariableSelection]
Pages   = ["forward_selection.jl"]
```
