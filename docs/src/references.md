# References

This package implements some of the variable selection methods mentioned
in:

["An Introduction to Statistical Learning with Applications in R"](https://api.semanticscholar.org/CorpusID:31693416)

This is a great reference for statistical learning/modeling. The selection
methods should be mentioned in Section 3.2.
