# VariableSelection.jl Documentation

## About

`VariableSelection.jl` is a Julia package for choosing which variables
to use when creating a statistical model using more automated methods.
It works with
[`GLM.jl`](https://github.com/JuliaStats/GLM.jl)
and the associate
[JuliaStats](https://juliastats.org/)
ecosystem.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/VariableSelection.jl
```

## Features

* Generating higher order and mixed variable terms
* Forward selection of variables
* Backward selection of variables
* Mixed selection of variables

## Related Packages

* [DataFrames.jl](https://github.com/JuliaData/DataFrames.jl) is a dependency used for working with tabular data
* [GLM.jl](https://github.com/JuliaStats/GLM.jl) is a dependency used for fitting to statistical models
