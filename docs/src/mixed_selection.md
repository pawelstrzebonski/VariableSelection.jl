# mixed_selection.jl

## Description

This file implements mixed selection of variables.

## Functions

```@autodocs
Modules = [VariableSelection]
Pages   = ["mixed_selection.jl"]
```
