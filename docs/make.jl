using Documenter
import VariableSelection

makedocs(
    sitename = "VariableSelection.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "VariableSelection.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "forward_selection.jl" => "forward_selection.md",
            "backward_selection.jl" => "backward_selection.md",
            "mixed_selection.jl" => "mixed_selection.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "forward_selection.jl" => "forward_selection_test.md",
            "backward_selection.jl" => "backward_selection_test.md",
            "mixed_selection.jl" => "mixed_selection_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
