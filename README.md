# VariableSelection

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/VariableSelection.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/VariableSelection.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/VariableSelection.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/VariableSelection.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/VariableSelection.jl/commits/master)

## About

`VariableSelection.jl` is a Julia package for choosing which variables
to use when creating a statistical model using more automated methods.
It works with
[`GLM.jl`](https://github.com/JuliaStats/GLM.jl)
and the associate
[JuliaStats](https://juliastats.org/)
ecosystem.

## Features

* Generating higher order and mixed variable terms
* Forward selection of variables
* Backward selection of variables
* Mixed selection of variables

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/VariableSelection.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for VariableSelection.jl](https://pawelstrzebonski.gitlab.io/VariableSelection.jl/).
